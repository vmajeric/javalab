package hr.java.vjezbe;

import java.io.IOException;
import java.util.OptionalLong;

import hr.java.vjezbe.baza.BazaPodataka;
import hr.java.vjezbe.entitet.Entitet;
import hr.java.vjezbe.entitet.PoslovniKorisnik;
import hr.java.vjezbe.iznimke.BazaPodatakaException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;

public class UnosPoslovnogKorisnikaController {

	@FXML
	private TextField nazivTextField;
	@FXML
	private TextField webTextField;
	@FXML
	private TextField emailTextField;
	@FXML
	private TextField telefonTextField;
	
	@FXML
	public void unesi() {
		
		StringBuilder sb=new StringBuilder();

		if(nazivTextField.getText().equals("")) sb.append("Polje naziv nije opcionalno.\n");
		if(webTextField.getText().equals("")) sb.append("Polje web nije opcionalno.\n");
		if(emailTextField.getText().equals("")) sb.append("Polje email nije opcionalno.\n");
		if(telefonTextField.getText().equals("")) sb.append("Polje telefon nije opcionalno.\n");
		
		String s= sb.toString();
		
		if(!s.equals("")) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška pri unosu podataka");
			alert.setHeaderText("Pogreška pri unosu podataka!");
			alert.setContentText(s);

			alert.showAndWait();
			return;
		}
		

		
		try {
			BazaPodataka.pohraniNovogPoslovnogKorisnika(new PoslovniKorisnik(
					this.nazivTextField.getText(),
					this.webTextField.getText(),
					this.emailTextField.getText(),
					this.telefonTextField.getText()
					));
		} catch (BazaPodatakaException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška");
			alert.setHeaderText(e.toString());
			alert.setContentText(e.getMessage());

			alert.showAndWait();
			e.printStackTrace();
		}
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Uspješan unos");
		alert.setHeaderText("Uspješan unos!");
		alert.setContentText("");

		alert.showAndWait();
		
	}
	

	@FXML
	public void prikaziPretraguAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Automobili.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguStanova() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Stanovi.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguUsluga() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Usluge.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguPrivatnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PrivatniKorisnici.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguPoslovnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PoslovniKorisnici.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosAutomobila.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosPoslovnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPoslovnogKorisnika.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosPrivatnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPrivatnogKorisnika.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosStana() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosStana.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosUsluge() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosUsluge.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Prodaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosProdaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
