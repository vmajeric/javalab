package hr.java.vjezbe.niti;

import hr.java.vjezbe.App;
import hr.java.vjezbe.baza.BazaPodataka;
import hr.java.vjezbe.entitet.Usluga;
import hr.java.vjezbe.iznimke.BazaPodatakaException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ZadnjaUslugaNit implements Runnable {
	
	
	
	@Override
	public void run() {

		try {
			Usluga zadnja = BazaPodataka.dohvatiZadnjuUslugu();
			//Main.setZadnjaUsluga(zadnja);
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Najnovija usluga!");
			alert.setHeaderText("Najnovija usluga!");
			alert.setContentText("Oglas: " + zadnja.tekstOglasa());

			alert.showAndWait();
			
		} catch (BazaPodatakaException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška");
			alert.setHeaderText(e.toString());
			alert.setContentText(e.getMessage());

			alert.showAndWait();
			e.printStackTrace();
		}
		
	}
	
}
