package hr.java.vjezbe;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;

public class LoginController {
	
	@FXML
	private TextField usernameTextField;
	
	@FXML
	private TextField passwordField;
	
	@FXML
	public void prijaviMe() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("IPrijava u JavaFX aplikaciju");
		alert.setHeaderText("Uspješna prijava!");
		alert.setContentText(usernameTextField.getText() + ", uspješno ste se prijavili u aplikaciju.");

		alert.showAndWait();
	}
}
