package hr.java.vjezbe;

import java.io.IOException;
import java.math.BigDecimal;

import hr.java.vjezbe.baza.BazaPodataka;
import hr.java.vjezbe.entitet.Automobil;
import hr.java.vjezbe.entitet.Stanje;
import hr.java.vjezbe.entitet.Usluga;
import hr.java.vjezbe.iznimke.BazaPodatakaException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ComboBox;

public class UnosUslugeController {

	@FXML
	private TextField naslovTextField;
	@FXML
	private TextField opisTextField;
	@FXML
	private TextField cijenaTextField;

	@FXML
	private ComboBox<Stanje> stanjeComboBox;

	private Usluga tempUsluga = null;

	public void initialize() {
		stanjeComboBox.getItems().setAll(Stanje.values());
		stanjeComboBox.setValue(Stanje.RABLJENO);
	}

	@FXML
	public void unesi() {

		StringBuilder sb = new StringBuilder();

		if (naslovTextField.getText().equals(""))
			sb.append("Polje naslov nije opcionalno.\n");
		if (opisTextField.getText().equals(""))
			sb.append("Polje opis nije opcionalno.\n");
		if (cijenaTextField.getText().equals(""))
			sb.append("Polje cijena nije opcionalno.\n");

		String s = sb.toString();

		if (!s.equals("")) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška pri unosu podataka");
			alert.setHeaderText("Pogreška pri unosu podataka!");
			alert.setContentText(s);

			alert.showAndWait();
			return;
		}

		/*
		if (this.tempUsluga == null) {

			try {
				BazaPodataka.pohraniNovuUslugu(new Usluga(naslovTextField.getText(), opisTextField.getText(),
						BigDecimal.valueOf(Double.parseDouble(cijenaTextField.getText())), stanjeComboBox.getValue()));
			} catch (BazaPodatakaException e) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Pogreška");
				alert.setHeaderText(e.toString());
				alert.setContentText(e.getMessage());

				alert.showAndWait();
				e.printStackTrace();
			}
		} else {
			try {
				BazaPodataka.izmijeniUslugu(new Usluga(
						naslovTextField.getText(), 
						opisTextField.getText(),
						BigDecimal.valueOf(Double.parseDouble(cijenaTextField.getText())), 
						stanjeComboBox.getValue(),
						this.tempUsluga.getId()));
			} catch (BazaPodatakaException e) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Pogreška");
				alert.setHeaderText(e.toString());
				alert.setContentText(e.getMessage());

				alert.showAndWait();
				e.printStackTrace();
			}
		}*/
		
		Thread nit = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					BazaPodataka.pohraniNovuUslugu(new Usluga(naslovTextField.getText(), opisTextField.getText(),
							BigDecimal.valueOf(Double.parseDouble(cijenaTextField.getText())), stanjeComboBox.getValue()));
					
					
					
					} catch (BazaPodatakaException e) {
					e.printStackTrace();
				}				
			}
		});
		nit.start();
		
		Alert uspjesno = new Alert(AlertType.INFORMATION);
		uspjesno.setTitle("Uspješan unos");
		uspjesno.setHeaderText("Uspješan unos!");
		uspjesno.setContentText("");

		uspjesno.showAndWait();

		

	}

	public void preuzmiUslugu(Usluga u) {
		this.tempUsluga = u;
		naslovTextField.setText(u.getNaslov());
		opisTextField.setText(u.getOpis());
		cijenaTextField.setText(u.getCijena().toString());
		stanjeComboBox.setValue(u.getStanje());

	}

	@FXML
	public void prikaziPretraguAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Automobili.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguStanova() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Stanovi.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguUsluga() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Usluge.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguPrivatnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PrivatniKorisnici.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguPoslovnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PoslovniKorisnici.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosAutomobila.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosPoslovnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPoslovnogKorisnika.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosPrivatnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPrivatnogKorisnika.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosStana() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosStana.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosUsluge() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosUsluge.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Prodaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosProdaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
