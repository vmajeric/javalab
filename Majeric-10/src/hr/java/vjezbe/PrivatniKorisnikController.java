package hr.java.vjezbe;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import hr.java.vjezbe.baza.BazaPodataka;
import hr.java.vjezbe.entitet.Automobil;
import hr.java.vjezbe.entitet.PoslovniKorisnik;
import hr.java.vjezbe.entitet.PrivatniKorisnik;
import hr.java.vjezbe.entitet.Stan;
import hr.java.vjezbe.entitet.Usluga;
import hr.java.vjezbe.iznimke.BazaPodatakaException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

public class PrivatniKorisnikController {

	private ObservableList<PrivatniKorisnik> korisnici;
	
	@FXML
	private TableView<PrivatniKorisnik> table;
	@FXML
	private TextField imeTextField;
	@FXML
	private TextField prezimeTextField;
	@FXML
	private TextField emailTextField;
	@FXML
	private TextField telefonTextField;

	
	@FXML
	private TableColumn<PrivatniKorisnik, String> imeColumn;
	@FXML
	private TableColumn<PrivatniKorisnik, String> prezimeColumn;
	@FXML
	private TableColumn<PrivatniKorisnik, String> emailColumn;
	@FXML
	private TableColumn<PrivatniKorisnik, String> telefonColumn;
	
	@FXML
	public void initialize () {
		try {
			List<PrivatniKorisnik> listItems;
			listItems = BazaPodataka.dohvatiPrivatneKorisnikePremaKriterijima(null);
			this.korisnici = FXCollections.observableArrayList(listItems);
			this.imeColumn.setCellValueFactory(new PropertyValueFactory<PrivatniKorisnik, String>("ime"));
			this.prezimeColumn.setCellValueFactory(new PropertyValueFactory<PrivatniKorisnik, String>("prezime"));
			this.emailColumn.setCellValueFactory(new PropertyValueFactory<PrivatniKorisnik, String>("email"));
			this.telefonColumn.setCellValueFactory(new PropertyValueFactory<PrivatniKorisnik, String>("telefon"));
			
			this.table.setItems(this.korisnici);
		} catch (BazaPodatakaException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška");
			alert.setHeaderText(e.toString());
			alert.setContentText(e.getMessage());

			alert.showAndWait();
			e.printStackTrace();
		}
				
		
		
	}
	
	
	@FXML
	public void prikaziPretraguAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Automobili.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguStanova() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Stanovi.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguUsluga() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Usluge.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguPrivatnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PrivatniKorisnici.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguPoslovnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PoslovniKorisnici.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosAutomobila.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosPoslovnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPoslovnogKorisnika.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosPrivatnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPrivatnogKorisnika.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosStana() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosStana.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosUsluge() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosUsluge.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Prodaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosProdaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@FXML
	public void pretrazi() {
		this.table.setItems(FXCollections.observableArrayList(this.korisnici.stream()
				.filter(a -> a.getIme().toLowerCase().contains(this.imeTextField.getText().toLowerCase()))
				.filter(a -> a.getPrezime().toLowerCase().contains(this.prezimeTextField.getText().toLowerCase()))
				.filter(a -> a.getEmail().toLowerCase().contains(this.emailTextField.getText().toLowerCase()))
				.filter(a -> a.getTelefon().toLowerCase().contains(this.telefonTextField.getText().toLowerCase()))
				.collect(Collectors.toList())
				));
	}
}
