package hr.java.vjezbe;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import hr.java.vjezbe.baza.BazaPodataka;
import hr.java.vjezbe.entitet.Automobil;
import hr.java.vjezbe.entitet.Usluga;
import hr.java.vjezbe.iznimke.BazaPodatakaException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class UslugeController {

	private ObservableList<Usluga> stanovi;
	
	@FXML
	private TableView<Usluga> table;
	@FXML
	private TextField naslovTextField;
	@FXML
	private TextField opisTextField;
	@FXML
	private TextField cijenaTextField;

	@FXML
	private TableColumn<Usluga, String> naslovColumn;
	@FXML
	private TableColumn<Usluga, String> opisColumn;
	@FXML
	private TableColumn<Usluga, String> cijenaColumn;
	
	@FXML
	public void initialize () {

		try {
			List<Usluga> listItems;
			listItems = BazaPodataka.dohvatiUslugePremaKriterijima(null);
			this.stanovi = FXCollections.observableArrayList(listItems);
			this.naslovColumn.setCellValueFactory(new PropertyValueFactory<Usluga, String>("naslov"));
			this.opisColumn.setCellValueFactory(new PropertyValueFactory<Usluga, String>("opis"));
			this.cijenaColumn.setCellValueFactory(new PropertyValueFactory<Usluga, String>("cijena"));
			this.table.setItems(this.stanovi);
		} catch (BazaPodatakaException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška");
			alert.setHeaderText(e.toString());
			alert.setContentText(e.getMessage());

			alert.showAndWait();
			e.printStackTrace();
		}
		
	}
	
	@FXML
	public void clickOnTable() {
		Usluga usluga= this.table.getSelectionModel().getSelectedItem();
		
        try {
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("UnosUsluge.fxml"));

			Parent root = loader.load();
			
	        UnosUslugeController uaController = loader.getController();
	        uaController.preuzmiUslugu(usluga);
	        
	        Stage stage = new Stage();
            stage.setScene(new Scene(root, 600, 500));
            stage.show();
	        
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        

	}
	
	
	@FXML
	public void prikaziPretraguAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Automobili.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguStanova() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Stanovi.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguUsluga() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Usluge.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguPrivatnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PrivatniKorisnici.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziPretraguPoslovnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PoslovniKorisnici.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosAutomobila.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosPoslovnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPoslovnogKorisnika.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosPrivatnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPrivatnogKorisnika.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosStana() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosStana.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosUsluge() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosUsluge.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Prodaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosProdaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	@FXML
	public void pretrazi() {
		this.table.setItems(FXCollections.observableArrayList(this.stanovi.stream()
				.filter(a -> a.getNaslov().toLowerCase().contains(this.naslovTextField.getText().toLowerCase()))
				.filter(a -> a.getOpis().toLowerCase().contains(this.opisTextField.getText().toLowerCase()))
				.filter(a -> a.getCijena().toString().toLowerCase().contains(this.cijenaTextField.getText().toLowerCase()))
				.collect(Collectors.toList())
				));
	}
}
