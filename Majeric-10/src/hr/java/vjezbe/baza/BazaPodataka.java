package hr.java.vjezbe.baza;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.java.vjezbe.App;
import hr.java.vjezbe.entitet.Artikl;
import hr.java.vjezbe.entitet.Automobil;
import hr.java.vjezbe.entitet.Korisnik;
import hr.java.vjezbe.entitet.PoslovniKorisnik;
import hr.java.vjezbe.entitet.PrivatniKorisnik;
import hr.java.vjezbe.entitet.Prodaja;
import hr.java.vjezbe.entitet.Stan;
import hr.java.vjezbe.entitet.Stanje;
import hr.java.vjezbe.entitet.Usluga;
import hr.java.vjezbe.iznimke.BazaPodatakaException;

public class BazaPodataka {

	private static final Logger logger = LoggerFactory.getLogger(App.class);

	private static final String DATABASE_FILE = "database.properties";

	private static Connection spajanjeNaBazu() throws BazaPodatakaException {

		Connection veza = null;
		Properties svojstva = new Properties();
		try {
			svojstva.load(new FileReader(DATABASE_FILE));
			String urlBazePodataka = svojstva.getProperty("bazaPodatakaUrl");
			String korisnickoIme = svojstva.getProperty("korisnickoIme");
			String lozinka = svojstva.getProperty("lozinka");
			veza = DriverManager.getConnection(urlBazePodataka, korisnickoIme, lozinka);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (veza == null)
			throw new BazaPodatakaException("Nemogu se spojiti na bazu");
		return veza;
	}

	public static List<Artikl> dohvatiArtikle() throws BazaPodatakaException {
		List<Artikl> artikli = new ArrayList<>();

		try (Connection connection = spajanjeNaBazu()) {

			StringBuilder sqlUpit = new StringBuilder(
					"SELECT distinct artikl.id as idArtikla, naslov, opis, cijena, snaga,\n"
							+ " kvadratura, stanje.naziv as stanje, tipArtikla.naziv as tipArtikla\n"
							+ "FROM artikl inner join\n" + " stanje on stanje.id = artikl.idStanje inner join\n"
							+ " tipArtikla on tipArtikla.id = artikl.idTipArtikla ");

			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Artikl artikl = null;
				if (resultSet.getString("tipArtikla").equals("Automobil")) {
					artikl = new Automobil(resultSet.getString("naslov"), resultSet.getString("opis"),
							resultSet.getBigDecimal("cijena"),
							Stanje.valueOf(resultSet.getString("stanje").toUpperCase()),
							resultSet.getBigDecimal("snaga"), resultSet.getLong("idArtikla"));
				} else if (resultSet.getString("tipArtikla").equals("Usluga")) {
					artikl = new Usluga(resultSet.getString("naslov"), resultSet.getString("opis"),
							resultSet.getBigDecimal("cijena"),
							Stanje.valueOf(resultSet.getString("stanje").toUpperCase()),
							resultSet.getLong("idArtikla"));
				} else if (resultSet.getString("tipArtikla").equals("Stan")) {
					artikl = new Stan(resultSet.getString("naslov"), resultSet.getString("opis"),
							resultSet.getBigDecimal("cijena"),
							Stanje.valueOf(resultSet.getString("stanje").toUpperCase()), resultSet.getInt("kvadratura"),
							resultSet.getLong("idArtikla"));
				}

				artikli.add(artikl);
			}
		} catch (SQLException | BazaPodatakaException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}

		return artikli;
	}

	public static List<Korisnik> dohvatiKorisnike() throws BazaPodatakaException {
		List<Korisnik> korisnici = new ArrayList<>();

		try (Connection connection = spajanjeNaBazu()) {

			StringBuilder sqlUpit = new StringBuilder(
					"SELECT distinct korisnik.id as idKorisnika, korisnik.naziv as nazivKorisnika, web, email,\n"
							+ " telefon, ime, prezime, tipKorisnika.naziv as tipKorisnika\n"
							+ "from korisnik inner join\n"
							+ " tipKorisnika on tipKorisnika.id = korisnik.idTipKorisnika ");

			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Korisnik korisnik = null;
				if (resultSet.getString("tipKorisnika").equals("PrivatniKorisnik")) {
					korisnik = new PrivatniKorisnik(resultSet.getString("ime"), resultSet.getString("prezime"),
							resultSet.getString("email"), resultSet.getString("telefon"),
							resultSet.getLong("idKorisnika"));
				} else if (resultSet.getString("tipKorisnika").equals("PoslovniKorisnik")) {
					korisnik = new PoslovniKorisnik(resultSet.getString("nazivKorisnika"), resultSet.getString("web"),
							resultSet.getString("telefon"), resultSet.getString("email"),
							resultSet.getLong("idKorisnika"));
				}

				korisnici.add(korisnik);
			}
		} catch (SQLException | BazaPodatakaException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}

		return korisnici;
	}

	public static void pohraniNovuProdaju(Prodaja prodaja) throws BazaPodatakaException {
		try (Connection connection = spajanjeNaBazu()) {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO prodaja(idArtikl, idKorisnik, datumObjave) " + "values (?, ?, ?);");
			preparedStatement.setLong(1, prodaja.getArtikl().getId());
			preparedStatement.setLong(2, prodaja.getKorisnik().getId());
			preparedStatement.setString(3, prodaja.getDatumObjave().toString());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
	}

	public static List<Prodaja> dohvatiProdajuPremaKriterijima(Prodaja prodaja) throws BazaPodatakaException {
		List<Prodaja> listaProdaje = new ArrayList<>();
		try (Connection connection = spajanjeNaBazu()) {
			StringBuilder sqlUpit = new StringBuilder(
					"select distinct korisnik.id as idKorisnika, tipKorisnika.naziv as tipKorisnika, \r\n"
							+ "korisnik.naziv as nazivKorisnika, web, email, telefon, \r\n"
							+ "korisnik.ime, korisnik.prezime, tipArtikla.naziv as tipArtikla,\r\n"
							+ "artikl.naslov, artikl.opis, artikl.cijena, artikl.kvadratura,\r\n"
							+ "artikl.snaga, stanje.naziv as stanje, prodaja.datumObjave, artikl.id as idArtikla\r\n"
							+ "from korisnik inner join \r\n"
							+ "tipKorisnika on tipKorisnika.id = korisnik.idTipKorisnika inner join\r\n"
							+ "prodaja on prodaja.idKorisnik = korisnik.id inner join\r\n"
							+ "artikl on artikl.id = prodaja.idArtikl inner join\r\n"
							+ "tipArtikla on tipArtikla.id = artikl.idTipArtikla inner join\r\n"
							+ "stanje on stanje.id = artikl.idStanje where 1=1");
			if (Optional.ofNullable(prodaja).isEmpty() == false) {
				if (Optional.ofNullable(prodaja.getArtikl()).isPresent())
					sqlUpit.append(" AND prodaja.idArtikl = " + prodaja.getArtikl().getId());

				if (Optional.ofNullable(prodaja.getKorisnik()).isPresent())
					sqlUpit.append(" AND prodaja.idArtikl = " + prodaja.getKorisnik().getId());

				if (Optional.ofNullable(prodaja.getDatumObjave()).isPresent()) {
					sqlUpit.append(" AND prodaja.datumObjave = '"
							+ prodaja.getDatumObjave().format(DateTimeFormatter.ISO_DATE) + "'");

				}
			}
			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Korisnik korisnik = null;
				if (resultSet.getString("tipKorisnika").equals("PrivatniKorisnik")) {
					korisnik = new PrivatniKorisnik(resultSet.getString("ime"), resultSet.getString("prezime"),
							resultSet.getString("email"), resultSet.getString("telefon"),
							resultSet.getLong("idKorisnika"));
				} else if (resultSet.getString("tipKorisnika").equals("PoslovniKorisnik")) {
					korisnik = new PoslovniKorisnik(resultSet.getString("nazivKorisnika"), resultSet.getString("web"),
							resultSet.getString("telefon"), resultSet.getString("email"),
							resultSet.getLong("idKorisnika"));
				}

				Artikl artikl = null;
				if (resultSet.getString("tipArtikla").equals("Automobil")) {
					artikl = new Automobil(resultSet.getString("naslov"), resultSet.getString("opis"),
							resultSet.getBigDecimal("cijena"),
							Stanje.valueOf(resultSet.getString("stanje").toUpperCase()),
							resultSet.getBigDecimal("snaga"), resultSet.getLong("idArtikla"));
				} else if (resultSet.getString("tipArtikla").equals("Usluga")) {
					artikl = new Usluga(resultSet.getString("naslov"), resultSet.getString("opis"),
							resultSet.getBigDecimal("cijena"),
							Stanje.valueOf(resultSet.getString("stanje").toUpperCase()),
							resultSet.getLong("idArtikla"));
				} else if (resultSet.getString("tipArtikla").equals("Stan")) {
					artikl = new Stan(resultSet.getString("naslov"), resultSet.getString("opis"),
							resultSet.getBigDecimal("cijena"),
							Stanje.valueOf(resultSet.getString("stanje").toUpperCase()), resultSet.getInt("kvadratura"),
							resultSet.getLong("idArtikla"));
				}

				Prodaja novaProdaja = new Prodaja(artikl, korisnik,
						resultSet.getTimestamp("datumObjave").toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
				listaProdaje.add(novaProdaja);
			}
		} catch (SQLException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
		return listaProdaje;
	}

	public static Prodaja dohvatiZadnjuProdaju() throws BazaPodatakaException {

		try (Connection connection = spajanjeNaBazu()) {

			StringBuilder sqlUpit = new StringBuilder(
					"SELECT DISTINCT korisnik.id as idKorisnika, tipKorisnika.naziv as tipKorisnika, "
							+ "korisnik.naziv as nazivKorisnika, web, email, telefon, "
							+ "korisnik.ime, korisnik.prezime, tipArtikla.naziv as tipArtikla, "
							+ "artikl.naslov, artikl.opis, artikl.cijena, artikl.kvadratura, "
							+ "artikl.snaga, stanje.naziv as stanje, prodaja.datumObjave, "
							+ "artikl.id as idArtikla " 
							+ "FROM korisnik INNER JOIN "
							+ "tipKorisnika ON tipKorisnika.id = korisnik.idTipKorisnika INNER JOIN "
							+ "prodaja ON prodaja.idKorisnik = korisnik.id INNER JOIN "
							+ "artikl ON artikl.id = prodaja.idArtikl INNER JOIN "
							+ "tipArtikla ON tipArtikla.id = artikl.idTipArtikla INNER JOIN "
							+ "stanje ON stanje.id = artikl.idStanje " 
							+ "ORDER BY datumObjave DESC " 
							+ "limit 1");

			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			
			
			resultSet.next();
			Korisnik korisnik = null;
			if (resultSet.getString("tipKorisnika").equals("PrivatniKorisnik")) {
				korisnik = new PrivatniKorisnik(resultSet.getString("ime"), resultSet.getString("prezime"),
						resultSet.getString("email"), resultSet.getString("telefon"), resultSet.getLong("idKorisnika"));
			} else if (resultSet.getString("tipKorisnika").equals("PoslovniKorisnik")) {
				korisnik = new PoslovniKorisnik(resultSet.getString("nazivKorisnika"), resultSet.getString("web"),
						resultSet.getString("telefon"), resultSet.getString("email"), resultSet.getLong("idKorisnika"));
			}

			Artikl artikl = null;
			if (resultSet.getString("tipArtikla").equals("Automobil")) {
				artikl = new Automobil(resultSet.getString("naslov"), resultSet.getString("opis"),
						resultSet.getBigDecimal("cijena"), Stanje.valueOf(resultSet.getString("stanje").toUpperCase()),
						resultSet.getBigDecimal("snaga"), resultSet.getLong("idArtikla"));
			} else if (resultSet.getString("tipArtikla").equals("Usluga")) {
				artikl = new Usluga(resultSet.getString("naslov"), resultSet.getString("opis"),
						resultSet.getBigDecimal("cijena"), Stanje.valueOf(resultSet.getString("stanje").toUpperCase()),
						resultSet.getLong("idArtikla"));
			} else if (resultSet.getString("tipArtikla").equals("Stan")) {
				artikl = new Stan(resultSet.getString("naslov"), resultSet.getString("opis"),
						resultSet.getBigDecimal("cijena"), Stanje.valueOf(resultSet.getString("stanje").toUpperCase()),
						resultSet.getInt("kvadratura"), resultSet.getLong("idArtikla"));
			}

			Prodaja prodaja = new Prodaja(artikl, korisnik,
					resultSet.getTimestamp("datumObjave").toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

			return prodaja;

		} catch (SQLException | BazaPodatakaException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}

	}
	
	public static Usluga dohvatiZadnjuUslugu() throws BazaPodatakaException {
		
		try(Connection connection = spajanjeNaBazu()){
			
			StringBuilder sqlUpit = new StringBuilder(
					"SELECT DISTINCT * "
					+ "FROM artikl INNER JOIN stanje ON stanje.id = artikl.idStanje "
					+ "INNER JOIN tipArtikla ON tipArtikla.id = artikl.idTipArtikla "
					+ "WHERE tipArtikla.naziv = 'Usluga' "
					+ "ORDER BY id DESC "
					+ "LIMIT 1; ");
			
			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			
			resultSet.next();
			
			Long id = resultSet.getLong("artikl.id");
			String naslov = resultSet.getString("artikl.naslov");
			String opis = resultSet.getString("artikl.opis");
			BigDecimal cijena = resultSet.getBigDecimal("artikl.cijena");
			String stanje = resultSet.getString("stanje.naziv");
			
			return new Usluga(naslov, opis, cijena, Stanje.valueOf(stanje.toUpperCase()), id);
			
			
		} catch (SQLException | BazaPodatakaException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
		
	}

	public static List<Stan> dohvatiStanovePremaKriterijima(Stan stan) throws BazaPodatakaException {
		List<Stan> listaStanova = new ArrayList<>();
		try (Connection connection = spajanjeNaBazu()) {
			StringBuilder sqlUpit = new StringBuilder(
					"SELECT distinct artikl.id, naslov, opis, cijena, kvadratura, stanje.naziv "
							+ "FROM artikl inner join stanje on stanje.id = artikl.idStanje "
							+ "inner join tipArtikla on tipArtikla.id = artikl.idTipArtikla WHERE tipArtikla.naziv = 'Stan'");
			if (Optional.ofNullable(stan).isEmpty() == false) {
				if (Optional.ofNullable(stan).map(Stan::getId).isPresent())
					sqlUpit.append(" AND artikl.id = " + stan.getId());
				if (Optional.ofNullable(stan.getNaslov()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND artikl.naslov LIKE '%" + stan.getNaslov() + "%'");
				if (Optional.ofNullable(stan.getOpis()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND artikl.opis LIKE '%" + stan.getOpis() + "%'");
				if (Optional.ofNullable(stan).map(Stan::getCijena).isPresent())
					sqlUpit.append(" AND artikl.cijena = " + stan.getCijena());
				if (Optional.ofNullable(stan).map(Stan::getKvadratura).isPresent())
					sqlUpit.append(" AND artikl.kvadratura = " + stan.getKvadratura());
			}
			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Long id = resultSet.getLong("id");
				String naslov = resultSet.getString("naslov");
				String opis = resultSet.getString("opis");
				BigDecimal cijena = resultSet.getBigDecimal("cijena");
				Integer kvadratura = resultSet.getInt("kvadratura");
				String stanje = resultSet.getString("naziv");
				Stan newStan = new Stan(naslov, opis, cijena, Stanje.valueOf(stanje.toUpperCase()), kvadratura, id);
				listaStanova.add(newStan);
			}
		} catch (SQLException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
		return listaStanova;
	}

	public static void pohraniNoviStan(Stan stan) throws BazaPodatakaException {
		try (Connection veza = spajanjeNaBazu()) {
			PreparedStatement preparedStatement = veza
					.prepareStatement("insert into artikl(Naslov, Opis, Cijena, Kvadratura, idStanje, idTipArtikla) "
							+ "values (?, ?, ?, ?, ?, 3);");
			preparedStatement.setString(1, stan.getNaslov());
			preparedStatement.setString(2, stan.getOpis());
			preparedStatement.setBigDecimal(3, stan.getCijena());
			preparedStatement.setInt(4, stan.getKvadratura());
			preparedStatement.setLong(5, (stan.getStanje().ordinal() + 1));
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, ex);
			throw new BazaPodatakaException(poruka, ex);
		}
	}

	public static List<Automobil> dohvatiAutomobilePremaKriterijima(Automobil auto) throws BazaPodatakaException {
		List<Automobil> listaAutomobila = new ArrayList<>();
		try (Connection connection = spajanjeNaBazu()) {
			StringBuilder sqlUpit = new StringBuilder(
					"SELECT distinct artikl.id, naslov, opis, cijena, snaga, stanje.naziv "
							+ "FROM artikl inner join stanje on stanje.id = artikl.idStanje "
							+ "inner join tipArtikla on tipArtikla.id = artikl.idTipArtikla WHERE tipArtikla.naziv = 'Automobil'");
			if (Optional.ofNullable(auto).isEmpty() == false) {
				if (Optional.ofNullable(auto).map(Automobil::getId).isPresent())
					sqlUpit.append(" AND artikl.id = " + auto.getId());
				if (Optional.ofNullable(auto.getNaslov()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND artikl.naslov LIKE '%" + auto.getNaslov() + "%'");
				if (Optional.ofNullable(auto.getOpis()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND artikl.opis LIKE '%" + auto.getOpis() + "%'");
				if (Optional.ofNullable(auto).map(Automobil::getCijena).isPresent())
					sqlUpit.append(" AND artikl.cijena = " + auto.getCijena());
				if (Optional.ofNullable(auto).map(Automobil::getSnagaKS).isPresent())
					sqlUpit.append(" AND artikl.snaga = " + auto.getSnagaKS());
			}
			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Long id = resultSet.getLong("id");
				String naslov = resultSet.getString("naslov");
				String opis = resultSet.getString("opis");
				BigDecimal cijena = resultSet.getBigDecimal("cijena");
				BigDecimal snaga = resultSet.getBigDecimal("snaga");
				String stanje = resultSet.getString("naziv");
				Automobil newAuto = new Automobil(naslov, opis, cijena, Stanje.valueOf(stanje.toUpperCase()), snaga,
						id);
				listaAutomobila.add(newAuto);
			}
		} catch (SQLException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
		return listaAutomobila;
	}

	public static void pohraniNoviAutomobil(Automobil auto) throws BazaPodatakaException {
		try (Connection veza = spajanjeNaBazu()) {
			PreparedStatement preparedStatement = veza
					.prepareStatement("insert into artikl(Naslov, Opis, Cijena, snaga, idStanje, idTipArtikla) "
							+ "values (?, ?, ?, ?, ?, 1);");
			preparedStatement.setString(1, auto.getNaslov());
			preparedStatement.setString(2, auto.getOpis());
			preparedStatement.setBigDecimal(3, auto.getCijena());
			preparedStatement.setBigDecimal(4, auto.getSnagaKS());
			preparedStatement.setLong(5, (auto.getStanje().ordinal() + 1));
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, ex);
			throw new BazaPodatakaException(poruka, ex);
		}
	}

	public static List<Usluga> dohvatiUslugePremaKriterijima(Usluga usluga) throws BazaPodatakaException {
		List<Usluga> listaUsluga = new ArrayList<>();
		try (Connection connection = spajanjeNaBazu()) {
			StringBuilder sqlUpit = new StringBuilder("SELECT distinct artikl.id, naslov, opis, cijena, stanje.naziv "
					+ "FROM artikl inner join stanje on stanje.id = artikl.idStanje "
					+ "inner join tipArtikla on tipArtikla.id = artikl.idTipArtikla WHERE tipArtikla.naziv = 'Usluga'");
			if (Optional.ofNullable(usluga).isEmpty() == false) {
				if (Optional.ofNullable(usluga).map(Usluga::getId).isPresent())
					sqlUpit.append(" AND artikl.id = " + usluga.getId());
				if (Optional.ofNullable(usluga.getNaslov()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND artikl.naslov LIKE '%" + usluga.getNaslov() + "%'");
				if (Optional.ofNullable(usluga.getOpis()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND artikl.opis LIKE '%" + usluga.getOpis() + "%'");
				if (Optional.ofNullable(usluga).map(Usluga::getCijena).isPresent())
					sqlUpit.append(" AND artikl.cijena = " + usluga.getCijena());
			}
			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Long id = resultSet.getLong("id");
				String naslov = resultSet.getString("naslov");
				String opis = resultSet.getString("opis");
				BigDecimal cijena = resultSet.getBigDecimal("cijena");
				String stanje = resultSet.getString("naziv");
				Usluga newUsluga = new Usluga(naslov, opis, cijena, Stanje.valueOf(stanje.toUpperCase()), id);
				listaUsluga.add(newUsluga);
			}
		} catch (SQLException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
		return listaUsluga;
	}

	public static void pohraniNovuUslugu(Usluga usluga) throws BazaPodatakaException {
		try (Connection veza = spajanjeNaBazu()) {
			PreparedStatement preparedStatement = veza.prepareStatement(
					"insert into artikl(Naslov, Opis, Cijena, idStanje, idTipArtikla) " + "values (?, ?, ?, ?, 2);");
			preparedStatement.setString(1, usluga.getNaslov());
			preparedStatement.setString(2, usluga.getOpis());
			preparedStatement.setBigDecimal(3, usluga.getCijena());
			preparedStatement.setLong(4, (usluga.getStanje().ordinal() + 1));
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, ex);
			throw new BazaPodatakaException(poruka, ex);
		}
	}

	public static void izmijeniUslugu(Usluga usluga) throws BazaPodatakaException {
		try (Connection veza = spajanjeNaBazu()) {
			PreparedStatement ps = veza
					.prepareStatement("UPDATE Artikl SET naslov = ?, opis = ?, cijena = ? WHERE id = ?;");
			ps.setString(1, usluga.getNaslov());
			ps.setString(2, usluga.getOpis());
			ps.setBigDecimal(3, usluga.getCijena());
			ps.setLong(4, usluga.getId());
		} catch (SQLException ex) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, ex);
			throw new BazaPodatakaException(poruka, ex);
		}
	}

	public static List<PoslovniKorisnik> dohvatiPoslovneKorisnikePremaKriterijima(PoslovniKorisnik pk)
			throws BazaPodatakaException {
		List<PoslovniKorisnik> listaPK = new ArrayList<>();
		try (Connection connection = spajanjeNaBazu()) {
			StringBuilder sqlUpit = new StringBuilder(
					"SELECT distinct korisnik.id, korisnik.naziv, web, telefon, email "
							+ "FROM korisnik inner join tipkorisnika ON tipkorisnika.id = korisnik.idtipkorisnika "
							+ "WHERE tipkorisnika.naziv = 'PoslovniKorisnik'");
			if (Optional.ofNullable(pk).isEmpty() == false) {
				if (Optional.ofNullable(pk).map(PoslovniKorisnik::getId).isPresent())
					sqlUpit.append(" AND korisnik.id = " + pk.getId());
				if (Optional.ofNullable(pk.getNaziv()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.naziv LIKE '%" + pk.getNaziv() + "%'");
				if (Optional.ofNullable(pk.getWeb()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.web LIKE '%" + pk.getWeb() + "%'");
				if (Optional.ofNullable(pk.getTelefon()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.telefon LIKE '%" + pk.getTelefon() + "%'");
				if (Optional.ofNullable(pk.getEmail()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.email LIKE '%" + pk.getEmail() + "%'");
			}
			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Long id = resultSet.getLong("id");
				String naziv = resultSet.getString("naziv");
				String web = resultSet.getString("web");
				String telefon = resultSet.getString("telefon");
				String email = resultSet.getString("email");
				PoslovniKorisnik newPK = new PoslovniKorisnik(naziv, web, email, telefon, id);
				listaPK.add(newPK);
			}
		} catch (SQLException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
		return listaPK;
	}

	public static void pohraniNovogPoslovnogKorisnika(PoslovniKorisnik pk) throws BazaPodatakaException {
		try (Connection veza = spajanjeNaBazu()) {
			PreparedStatement preparedStatement = veza.prepareStatement(
					"insert into korisnik(Naziv, web, telefon, email, idTipKorisnika) " + "values (?, ?, ?, ?, 2);");
			preparedStatement.setString(1, pk.getNaziv());
			preparedStatement.setString(2, pk.getWeb());
			preparedStatement.setString(3, pk.getTelefon());
			preparedStatement.setString(4, pk.getEmail());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, ex);
			throw new BazaPodatakaException(poruka, ex);
		}
	}

	public static List<PrivatniKorisnik> dohvatiPrivatneKorisnikePremaKriterijima(PrivatniKorisnik pk)
			throws BazaPodatakaException {
		List<PrivatniKorisnik> listaPK = new ArrayList<>();
		try (Connection connection = spajanjeNaBazu()) {
			StringBuilder sqlUpit = new StringBuilder("SELECT distinct korisnik.id, ime, prezime, telefon, email "
					+ "FROM korisnik inner join tipkorisnika ON tipkorisnika.id = korisnik.idtipkorisnika "
					+ "WHERE tipkorisnika.naziv = 'PrivatniKorisnik'");
			if (Optional.ofNullable(pk).isEmpty() == false) {
				if (Optional.ofNullable(pk).map(PrivatniKorisnik::getId).isPresent())
					sqlUpit.append(" AND korisnik.id = " + pk.getId());
				if (Optional.ofNullable(pk.getIme()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.ime LIKE '%" + pk.getIme() + "%'");
				if (Optional.ofNullable(pk.getPrezime()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.prezime LIKE '%" + pk.getPrezime() + "%'");
				if (Optional.ofNullable(pk.getTelefon()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.telefon LIKE '%" + pk.getTelefon() + "%'");
				if (Optional.ofNullable(pk.getEmail()).map(String::isBlank).orElse(true) == false)
					sqlUpit.append(" AND korisnik.email LIKE '%" + pk.getEmail() + "%'");
			}
			Statement query = connection.createStatement();
			ResultSet resultSet = query.executeQuery(sqlUpit.toString());
			while (resultSet.next()) {
				Long id = resultSet.getLong("id");
				String ime = resultSet.getString("ime");
				String prezime = resultSet.getString("prezime");
				String telefon = resultSet.getString("telefon");
				String email = resultSet.getString("email");
				PrivatniKorisnik newPK = new PrivatniKorisnik(ime, prezime, email, telefon, id);
				listaPK.add(newPK);
			}
		} catch (SQLException e) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, e);
			throw new BazaPodatakaException(poruka, e);
		}
		return listaPK;
	}

	public static void pohraniNovogPrivatnogKorisnika(PrivatniKorisnik pk) throws BazaPodatakaException {
		try (Connection veza = spajanjeNaBazu()) {
			PreparedStatement preparedStatement = veza.prepareStatement(
					"insert into korisnik(ime, prezime, telefon, email, idTipKorisnika) " + "values (?, ?, ?, ?, 1);");
			preparedStatement.setString(1, pk.getIme());
			preparedStatement.setString(2, pk.getPrezime());
			preparedStatement.setString(3, pk.getTelefon());
			preparedStatement.setString(4, pk.getEmail());
			preparedStatement.executeUpdate();
		} catch (SQLException ex) {
			String poruka = "Došlo je do pogreške u radu s bazom podataka";
			logger.error(poruka, ex);
			throw new BazaPodatakaException(poruka, ex);
		}
	}

}
