package hr.java.vjezbe;


import hr.java.vjezbe.entitet.Usluga;
import hr.java.vjezbe.niti.DatumObjaveNit;
import hr.java.vjezbe.niti.ZadnjaUslugaNit;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

public class App extends Application {

	private static BorderPane root;
	private static Stage stage;
	
	private static Usluga zadnjaUsluga=null;
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	public static void setZadnjaUsluga(Usluga u) {
		synchronized(App.zadnjaUsluga){
			App.zadnjaUsluga=u;
		}
	}
	
	public static Usluga getZadnjaUsluga() {
		synchronized(App.zadnjaUsluga) {
			return App.zadnjaUsluga;
		}
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			root = (BorderPane) FXMLLoader.load(getClass().getResource("Pocetni.fxml"));
			Scene scene = new Scene(root, 200, 200);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			stage = primaryStage;
			
			
			/*
			Timeline prikazSlavljenika = new Timeline(
					new KeyFrame(Duration.seconds(10), new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							Platform.runLater(new DatumObjaveNit());
						}
					}));
			prikazSlavljenika.setCycleCount(Timeline.INDEFINITE);
			prikazSlavljenika.play();
			*/
			
			Timeline prikazUsluge = new Timeline(
					new KeyFrame(Duration.seconds(10), new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							Platform.runLater(new ZadnjaUslugaNit());
						}
					}));
			prikazUsluge.setCycleCount(Timeline.INDEFINITE);
			prikazUsluge.play();
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void setMainPage(BorderPane root) {

		Scene scene = new Scene(root, 600, 500);

		stage.setScene(scene);
		stage.show();
	}

	

	public static void setCenterPane(BorderPane center) {

		setMainPage(center);

	}
}
