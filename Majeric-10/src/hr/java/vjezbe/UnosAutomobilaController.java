package hr.java.vjezbe;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.OptionalLong;

import hr.java.vjezbe.baza.BazaPodataka;
import hr.java.vjezbe.entitet.Automobil;
import hr.java.vjezbe.entitet.Entitet;
import hr.java.vjezbe.entitet.Stanje;
import hr.java.vjezbe.iznimke.BazaPodatakaException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ComboBox;

public class UnosAutomobilaController {
	
	private Automobil tempAuto=null;

	@FXML
	private TextField naslovTextField;
	@FXML
	private TextField opisTextField;
	@FXML
	private TextField snagaTextField;
	@FXML
	private TextField cijenaTextField;

	@FXML
	private ComboBox<Stanje> stanjeComboBox;

	public void initialize() {
		stanjeComboBox.getItems().setAll(Stanje.values());
		stanjeComboBox.setValue(Stanje.RABLJENO);
	}
	
	public void preuzmiAutomobil(Automobil a) {
		this.tempAuto=a;
		naslovTextField.setText(a.getNaslov());
		opisTextField.setText(a.getOpis());
		snagaTextField.setText(a.getSnagaKS().toString());
		cijenaTextField.setText(a.getCijena().toString());
		stanjeComboBox.setValue(a.getStanje());
		
	}
	

	@FXML
	public void unesi() {

		StringBuilder sb = new StringBuilder();

		if (naslovTextField.getText().equals("")) {
			sb.append("Polje naslov nije opcionalno.\n");
			naslovTextField.setStyle("-fx-background-color: red;");
		} else {
			naslovTextField.setStyle("");
		}
		if (opisTextField.getText().equals("")) {
			sb.append("Polje opis nije opcionalno.\n");
			opisTextField.setStyle("-fx-background-color: red;");
		} else {
			opisTextField.setStyle("");
		}
		if (snagaTextField.getText().equals("")) {
			sb.append("Polje snaga nije opcionalno.\n");
			snagaTextField.setStyle("-fx-background-color: red;");
		} else {
			snagaTextField.setStyle("");
		}
		if (cijenaTextField.getText().equals("")) {
			sb.append("Polje cijena nije opcionalno.\n");
			cijenaTextField.setStyle("-fx-background-color: red;");
		} else {
			cijenaTextField.setStyle("");
		}

		String s = sb.toString();

		if (!s.equals("")) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška pri unosu podataka");
			alert.setHeaderText("Pogreška pri unosu podataka!");
			alert.setContentText(s);

			alert.showAndWait();
			return;
		}

		try {
			BazaPodataka.pohraniNoviAutomobil(new Automobil(
					naslovTextField.getText(), 
					opisTextField.getText(),
					BigDecimal.valueOf(Double.parseDouble(cijenaTextField.getText())), 
					stanjeComboBox.getValue(),
					BigDecimal.valueOf(Double.parseDouble(snagaTextField.getText()))
					));
		} catch (BazaPodatakaException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pogreška");
			alert.setHeaderText(e.toString());
			alert.setContentText(e.getMessage());

			alert.showAndWait();
			e.printStackTrace();
		}
		
		
		

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Uspješan unos");
		alert.setHeaderText("Uspješan unos!");
		alert.setContentText("");

		alert.showAndWait();

	}

	@FXML
	public void prikaziPretraguAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Automobili.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguStanova() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Stanovi.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguUsluga() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Usluge.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguPrivatnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PrivatniKorisnici.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziPretraguPoslovnihKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("PoslovniKorisnici.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosAutomobila() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosAutomobila.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosPoslovnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPoslovnogKorisnika.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosPrivatnogKorisnika() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosPrivatnogKorisnika.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosStana() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosStana.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void prikaziUnosUsluge() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosUsluge.fxml"));
			App.setCenterPane(center);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("Prodaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void prikaziUnosProdaje() {
		try {
			BorderPane center = FXMLLoader.load(getClass().getResource("UnosProdaje.fxml"));
			App.setCenterPane(center);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
