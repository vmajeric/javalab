package hr.java.vjezbe.entitet;

import java.math.BigDecimal;

import hr.java.vjezbe.iznimke.CijenaJePreniskaException;
/**
 * Represents an immovable good.
 * @author Vjekoslav Majerić
 *
 */
public interface Nekretnina {

	public static int minValue=10000;
	/**
	 * Calculates the amount of tax when bought.
	 * @param cijena Price
	 * @return amount of tax
	 */
	public default BigDecimal izracunajPorez(BigDecimal cijena) {
		if (cijena.intValue()<minValue) throw new CijenaJePreniskaException("Preniska cijena nekretnine");
		return cijena.multiply(BigDecimal.valueOf(0.03));
	}
}
