package hr.java.vjezbe.entitet;

import java.math.BigDecimal;
/**
 * Represents a service.
 * @author Vjekoslav Majerić
 *
 */
public class Usluga extends Artikl{

	
	/**
	 * Constructor.
	 * @param naslov title
	 * @param opis description
	 * @param cijena price
	 */
	public Usluga(String naslov, String opis, BigDecimal cijena, Stanje stanje) {
		super(naslov, opis, cijena, stanje);
	}
	
	
	public Usluga(String naslov, String opis, BigDecimal cijena, Stanje stanje, long id) {
		super(naslov, opis, cijena, stanje, id);
	}
	
	

	public Usluga(String naslov, String opis, BigDecimal cijena, Stanje stanje, Long id) {
		super(naslov, opis, cijena, stanje, id);
	}

	@Override
	public String tekstOglasa() {
		StringBuilder output = new StringBuilder();
		output.append("Naslov usluge: ");
		output.append(this.getNaslov());
		output.append("\nOpis usluge: ");
		output.append(this.getOpis());
		output.append("\nCijena usluge: ");
		output.append(this.getCijena());
		output.append("\nStanje usluge: ");
		output.append(this.getStanje().toString());
		
		return output.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		output.append(this.getNaslov());
		output.append(", ");
		output.append(this.getOpis());
		output.append(", Cijena: ");
		output.append(this.getCijena());
		
		return output.toString();
	}
	
	

}
