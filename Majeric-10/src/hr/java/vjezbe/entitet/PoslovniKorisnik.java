package hr.java.vjezbe.entitet;

/**
 * Represents a company.
 * @author Vjekoslav Majerić
 *
 */
public class PoslovniKorisnik extends Korisnik {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8819793563722213050L;
	private String naziv;
	private String web;
	
	/**
	 * Constructor.
	 * @param naziv company name
	 * @param web company's webpage
	 * @param email company's e-mail adress
	 * @param telefon company's phone number
	 */
	public PoslovniKorisnik(String naziv, String web, String email, String telefon, long id) {
		super(email, telefon, id);
		this.naziv = naziv;
		this.web = web;
	}
	
	public PoslovniKorisnik(String naziv, String web, String email, String telefon) {
		super(email, telefon);
		this.naziv = naziv;
		this.web = web;
	}

	/**
	 * Getter for <code>naziv</code>
	 * @return <code>naziv</code> as <code>String</code>
	 */
	public String getNaziv() {
		return naziv;
	}

	/**
	 * Setter for <code>naziv</code>
	 * @param naslov new <code>naziv</code> value
	 */
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	/**
	 * Getter for <code>web</code>
	 * @return <code>web</code> as <code>String</code>
	 */
	public String getWeb() {
		return web;
	}

	/**
	 * Setter for <code>web</code>
	 * @param naslov new <code>web</code> value
	 */
	public void setWeb(String web) {
		this.web = web;
	}

	

	@Override
	public String dohvatiKontakt() {
		StringBuilder output = new StringBuilder();
		output.append("Naziv tvrtke: ");
		output.append(this.getNaziv());
		output.append(", mail: ");
		output.append(this.getEmail());
		output.append(", tel: ");
		output.append(this.getTelefon());
		output.append(", web: ");
		output.append(this.getWeb());
		return output.toString();
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		output.append(this.getNaziv());
		output.append(", ");
		output.append(this.getEmail());
		output.append(", tel: ");
		output.append(this.getTelefon());
		output.append(", ");
		output.append(this.getWeb());
		return output.toString();
	}
}
