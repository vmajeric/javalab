package hr.java.vjezbe.entitet;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.java.vjezbe.App;
import hr.java.vjezbe.iznimke.CijenaJePreniskaException;
/**
 * Represents an apartment.
 * @author Vjekoslav Majerić
 *
 */
public class Stan extends Artikl implements Nekretnina{
	
	private static final Logger logger = LoggerFactory.getLogger(App.class);

	private int kvadratura;

	/**
	 * Constructor.
	 * @param naslov title 
	 * @param opis description
	 * @param cijena price
	 * @param kvadratura size in meters squared
	 */
	public Stan(String naslov, String opis, BigDecimal cijena, Stanje stanje, int kvadratura) {
		super(naslov, opis, cijena, stanje);
		this.kvadratura = kvadratura;
	}
	
	public Stan(String naslov, String opis, BigDecimal cijena, Stanje stanje, int kvadratura, Long id) {
		super(naslov, opis, cijena, stanje, id);
		this.kvadratura = kvadratura;
	}

	/**
	 * Getter for <code>kvadratura</code>
	 * @return <code>kvadratura</code> as <code>int</code>
	 */
	public int getKvadratura() {
		return kvadratura;
	}

	/**
	 * Setter for <code>kvadratura</code>
	 * @param naslov new <code>kvadratura</code> value
	 */
	public void setKvadratura(int kvadratura) {
		this.kvadratura = kvadratura;
	}
	
	
	@Override
	public String tekstOglasa() {
		StringBuilder output = new StringBuilder();
		output.append("Naslov stana: ");
		output.append(this.getNaslov());
		output.append("\nOpis stana: ");
		output.append(this.getOpis());
		output.append("\nCijena stana: ");
		output.append(this.getCijena());
		output.append("\nPorez: ");
		try {
		output.append(this.izracunajPorez(this.getCijena()));
		} catch(CijenaJePreniskaException e) {
			output.append("Cijena je preniska za izračun");
			logger.info(e.getMessage(), e);
		}
		output.append("\nKvadratura stana: ");
		output.append(this.getKvadratura());
		output.append("\nStanje stana: ");
		output.append(this.getStanje().toString());
		
		return output.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		output.append(this.getNaslov());
		output.append(", ");
		output.append(this.getOpis());
		output.append(", ");
		output.append(this.getKvadratura());
		output.append(", Cijena: ");
		output.append(this.getCijena());
		output.append(", Stanje: ");
		output.append(this.getStanje().toString());
		
		return output.toString();
	}
	

}
