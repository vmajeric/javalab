package hr.java.vjezbe.entitet;

import java.util.List;

public class Kategorija<T extends Artikl>{

	private String naziv;
	private List<T> artikli;

	public Kategorija(String naziv, List<T> artikli) {
		super();
		this.naziv = naziv;
		this.artikli = artikli;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<T> getArtikli() {
		return artikli;
	}

	public void setArtikli(List<T> artikli) {
		this.artikli = artikli;
	}
	
	public void dodajArtikl(T artikl) {
		this.artikli.add(artikl);
	}
	
	public T dohvatiArtikl(int index) {
		return this.getArtikli().get(index);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kategorija other = (Kategorija) obj;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		return true;
	}

	
	
	

}
