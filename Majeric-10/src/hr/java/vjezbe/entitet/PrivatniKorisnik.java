package hr.java.vjezbe.entitet;

/**
 * Represents a person.
 * @author Vjekoslav Majerić
 *
 */
public class PrivatniKorisnik extends Korisnik {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7405559510336766712L;
	private String ime;
	private String prezime;

	/**
	 * Constructor.
	 * @param ime name
	 * @param prezime surname
	 * @param email e-mail
	 * @param telefon phone number
	 */
	public PrivatniKorisnik(String ime, String prezime, String email, String telefon, long id) {
		super(email, telefon, id);
		this.ime = ime;
		this.prezime = prezime;
	}
	
	public PrivatniKorisnik(String ime, String prezime, String email, String telefon) {
		super(email, telefon);
		this.ime = ime;
		this.prezime = prezime;
	}

	/**
	 * Getter for <code>ime</code>
	 * @return <code>ime</code> as <code>String</code>
	 */
	public String getIme() {
		return ime;
	}

	/**
	 * Setter for <code>ime</code>
	 * @param naslov new <code>ime</code> value
	 */
	public void setIme(String ime) {
		this.ime = ime;
	}

	/**
	 * Getter for <code>prezime</code>
	 * @return <code>prezime</code> as <code>String</code>
	 */
	public String getPrezime() {
		return prezime;
	}

	/**
	 * Setter for <code>prezime</code>
	 * @param naslov new <code>prezime</code> value
	 */
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	@Override
	public String dohvatiKontakt() {
		StringBuilder output = new StringBuilder();
		output.append("Osobni podaci prodavatelja: ");
		output.append(this.getIme());
		output.append(" ");
		output.append(this.getPrezime());
		output.append(", mail: ");
		output.append(this.getEmail());
		output.append(", tel: ");
		output.append(this.getTelefon());
		return output.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		output.append(this.getIme());
		output.append(" ");
		output.append(this.getPrezime());
		output.append(", ");
		output.append(this.getEmail());
		output.append(", tel: ");
		output.append(this.getTelefon());
		return output.toString();
	}

}
