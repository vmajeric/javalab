package hr.java.vjezbe.entitet;
/**
 * Represents state of the item.
 * @author Vjekoslav Majerić
 *
 */
public enum Stanje {
	NOVO , IZVRSNO, RABLJENO, NEISPRAVNO
}
