package hr.java.vjezbe.entitet;

import java.io.Serializable;

/**
 * Abstract class that represents a user.
 * @author Vjekoslav Majerić
 *
 */
public abstract class Korisnik extends Entitet{

	private String email;
	private String telefon;
	/**
	 * Constructor.
	 * @param email user's e-mail
	 * @param telefon user's phone number
	 */
	public Korisnik(String email, String telefon, long id) {
		super(id);
		this.email = email;
		this.telefon = telefon;
	}
	
	public Korisnik(String email, String telefon) {
		super();
		this.email = email;
		this.telefon = telefon;
	}

	/**
	 * Getter for <code>email</code>
	 * @return <code>email</code> as <code>String</code>
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter for <code>email</code>
	 * @param naslov new <code>email</code> value
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter for <code>telefon</code>
	 * @return <code>telefon</code> as <code>String</code>
	 */
	public String getTelefon() {
		return telefon;
	}

	/**
	 * Setter for <code>telefon</code>
	 * @param naslov new <code>telefon</code> value
	 */
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	/**
	 * Returns user's contact information.
	 * @return user's contact information
	 */
	public abstract String dohvatiKontakt();
	
}
