package hr.java.vjezbe.entitet;

import java.math.BigDecimal;

import hr.java.vjezbe.iznimke.NemoguceOdreditiGrupuOsiguranjaException;
/**
 * Represents a vehicle.
 * @author  Vjekoslav Majerić
 *
 */
public interface Vozilo {

	/**
	 * Calculates power in kw from hp.
	 * @param hp power in hp
	 * @return power in kw
	 */
	public default BigDecimal izracunajKw(BigDecimal hp) {

		return hp.multiply(BigDecimal.valueOf(0.745699872));
	}

	/**
	 * Calculates insurance group.
	 * @return number of insurance group (1-5) 
	 * @throws NemoguceOdreditiGrupuOsiguranjaException
	 */
	public BigDecimal izracunajGrupuOsiguranja() throws NemoguceOdreditiGrupuOsiguranjaException;

	/**
	 * Calculates insurance price
	 * @return insurance price
	 * @throws NemoguceOdreditiGrupuOsiguranjaException
	 */
	public default BigDecimal izracunajCijenuOsiguranja() throws NemoguceOdreditiGrupuOsiguranjaException {
		BigDecimal grupa = this.izracunajGrupuOsiguranja();
		BigDecimal cijena;
		switch (grupa.intValue()) {
		case 1:
			cijena = BigDecimal.valueOf(100);
			break;
		case 2:
			cijena = BigDecimal.valueOf(200);
			break;
		case 3:
			cijena = BigDecimal.valueOf(300);
			break;
		case 4:
			cijena = BigDecimal.valueOf(400);
			break;
		case 5:
			cijena = BigDecimal.valueOf(500);
			break;
		default:
			cijena = BigDecimal.valueOf(0);
			break;
		}
		;
		return cijena;
	}
}
