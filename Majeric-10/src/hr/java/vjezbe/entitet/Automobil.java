package hr.java.vjezbe.entitet;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.java.vjezbe.App;
import hr.java.vjezbe.iznimke.NemoguceOdreditiGrupuOsiguranjaException;
/**
 * Represents a car.
 * @author Vjekoslav Majerić
 *
 */
public class Automobil extends Artikl implements Vozilo{
	

	private static final Logger logger = LoggerFactory.getLogger(App.class);
	
	private BigDecimal snagaKS;

	/**
	 * Constructor.
	 * @param naslov Title
	 * @param opis	Description
	 * @param cijena Price
	 * @param snagaKS Horsepower
	 */
	public Automobil(String naslov, String opis, BigDecimal cijena, Stanje stanje, BigDecimal snagaKS) {
		super(naslov, opis, cijena, stanje);
		this.snagaKS = snagaKS;
	}
	
	public Automobil(String naslov, String opis, BigDecimal cijena, Stanje stanje, BigDecimal snagaKS, Long id) {
		super(naslov, opis, cijena, stanje, id);
		this.snagaKS = snagaKS;
	}

	/**
	 * Getter for <code>snagaKS</code>
	 * @return <code>snagaKS</code> as <code>BigDecimal</code>
	 */
	public BigDecimal getSnagaKS() {
		return snagaKS;
	}

	/**
	 * Setter for <code>snagaKS</code>
	 * @param naslov new <code>snagaKS</code> value
	 */
	public void setSnagaKS(BigDecimal snagaKS) {
		this.snagaKS = snagaKS;
	}

	@Override
	public BigDecimal izracunajGrupuOsiguranja() throws NemoguceOdreditiGrupuOsiguranjaException{
		int snaga=-1;
		try {
		snaga=this.getSnagaKS().intValue();
		} catch (Exception e) {
			throw new NemoguceOdreditiGrupuOsiguranjaException(e);
		}
		if(snaga<0) return BigDecimal.valueOf(5);
		if(snaga<80) return BigDecimal.valueOf(1);
		if(snaga<100) return BigDecimal.valueOf(2);
		if(snaga<120) return BigDecimal.valueOf(3);
		if(snaga<150) return BigDecimal.valueOf(4);
		return BigDecimal.valueOf(5);
	}

	@Override
	public String tekstOglasa() {
		StringBuilder output = new StringBuilder();
		output.append("Naslov automobila: ");
		output.append(this.getNaslov());
		output.append("\nOpis automobila: ");
		output.append(this.getOpis());
		output.append("\nSnaga automobila: ");
		output.append(this.izracunajKw(this.getSnagaKS()));
		output.append("\nIzračun osiguranja automobila: ");
		try {
			output.append(this.izracunajCijenuOsiguranja());
		} catch (NemoguceOdreditiGrupuOsiguranjaException e) {
			output.append("Nemoguće odrediti grupu osiguranja");
			logger.info(e.getMessage(), e);
		}
		output.append("\nCijena automobila: ");
		output.append(this.getCijena());
		output.append("\nStanje automobila: ");
		output.append(this.getStanje().toString());
		
		return output.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		output.append(this.getNaslov());
		output.append(", ");
		output.append(this.getOpis());
		output.append(", ");
		output.append(this.getSnagaKS());
		output.append(" KS, Cijena: ");
		output.append(this.getCijena());
		output.append(", Stanje: ");
		output.append(this.getStanje().toString());
		
		return output.toString();
	}
	
	

}
