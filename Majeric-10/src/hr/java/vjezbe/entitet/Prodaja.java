package hr.java.vjezbe.entitet;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/**
 * Represents an add.
 * @author Vjekoslav Majerić
 *
 */
public class Prodaja implements Serializable{

	private Artikl artikl;
	private Korisnik korisnik;
	private LocalDate datumObjave;

	/**
	 * Constructor.
	 * 
	 * @param artikl item for sale
	 * @param korisnik seller
	 * @param datumObjave date of add creation
	 */
	public Prodaja(Artikl artikl, Korisnik korisnik, LocalDate datumObjave) {
		super();
		this.artikl = artikl;
		this.korisnik = korisnik;
		this.datumObjave = datumObjave;
	}

	/**
	 * Getter for <code>artikl</code>
	 * @return <code>artikl</code> as <code>Artikl</code>
	 */
	public Artikl getArtikl() {
		return artikl;
	}

	/**
	 * Setter for <code>artikl</code>
	 * @param naslov new <code>artikl</code> value
	 */
	public void setArtikl(Artikl artikl) {
		this.artikl = artikl;
	}

	/**
	 * Getter for <code>korisnik</code>
	 * @return <code>korisnik</code> as <code>Korisnik</code>
	 */
	public Korisnik getKorisnik() {
		return korisnik;
	}

	/**
	 * Setter for <code>korisnik</code>
	 * @param naslov new <code>korisnik</code> value
	 */
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	/**
	 * Getter for <code>datumObjave</code>
	 * @return <code>datumObjave</code> as <code>LocalDate</code>
	 */
	public LocalDate getDatumObjave() {
		return datumObjave;
	}

	/**
	 * Setter for <code>datumObjave</code>
	 * @param naslov new <code>datumObjave</code> value
	 */
	public void setDatumObjave(LocalDate datumObjave) {
		this.datumObjave = datumObjave;
	}

	@Override
	public String toString() {
		StringBuilder output=new StringBuilder();
		output.append(this.getArtikl().tekstOglasa());
		output.append("\n");
		output.append("Datum objave: ");
		output.append(this.getDatumObjave().format(DateTimeFormatter.ofPattern("dd.MM.YYYY")));
		output.append("\n");
		output.append(this.getKorisnik().dohvatiKontakt());
		output.append("\n");
		return output.toString();
	}

	
}
