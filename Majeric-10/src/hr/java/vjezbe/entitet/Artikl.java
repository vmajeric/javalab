package hr.java.vjezbe.entitet;

import java.math.BigDecimal;

/**
 * Abstract class that represents an item or service.
 * 
 * @author Vjekoslav Majerić
 *
 */
public abstract class Artikl extends Entitet {
	private String naslov;
	private String opis;
	private BigDecimal cijena;
	private Stanje stanje;

	/**
	 * Constructor.
	 * 
	 * @param naslov Title
	 * @param opis   Description
	 * @param cijena Price
	 */
	public Artikl(String naslov, String opis, BigDecimal cijena, Stanje stanje) {
		super();
		this.naslov = naslov;
		this.opis = opis;
		this.cijena = cijena;
		this.stanje = stanje;
	}
	
	public Artikl(String naslov, String opis, BigDecimal cijena, Stanje stanje, Long id) {
		super(id);
		this.naslov = naslov;
		this.opis = opis;
		this.cijena = cijena;
		this.stanje = stanje;
	}

	/**
	 * Getter for <code>naslov</code>
	 * 
	 * @return <code>naslov</code> as <code>String</code>
	 */
	public String getNaslov() {
		return naslov;
	}

	/**
	 * Setter for <code>naslov</code>
	 * 
	 * @param naslov new <code>naslov</code> value
	 */
	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	/**
	 * Getter for <code>opis</code>
	 * 
	 * @return <code>opis</code> as <code>String</code>
	 */
	public String getOpis() {
		return opis;
	}

	/**
	 * Setter for <code>opis</code>
	 * 
	 * @param naslov new <code>opis</code> value
	 */
	public void setOpis(String opis) {
		this.opis = opis;
	}

	/**
	 * Getter for <code>cijena</code>
	 * 
	 * @return <code>cijena</code> as <code>BigDecimal</code>
	 */
	public BigDecimal getCijena() {
		return cijena;
	}

	/**
	 * Setter for <code>cijena</code>
	 * 
	 * @param naslov new <code>cijena</code> value
	 */
	public void setCijena(BigDecimal cijena) {
		this.cijena = cijena;
	}

	public Stanje getStanje() {
		return stanje;
	}

	public void setStanje(Stanje stanje) {
		this.stanje = stanje;
	}

	/**
	 * Generates text of the add.
	 * 
	 * @return text of the add as <code>String</code>
	 */
	public abstract String tekstOglasa();

	
	

	
}
