package hr.java.vjezbe.iznimke;

/**
 * Thrown if price is set too low.
 * 
 * @author Vjekoslav Majerić
 *
 */
public class CijenaJePreniskaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2555341954833609776L;

	/**
	 * Constructor.
	 */
	public CijenaJePreniskaException() {
		super("Dogodila se pogreška u radu programa!");
	}

	/**
	 * Constructor.
	 * @param message
	 * @param cause
	 */
	public CijenaJePreniskaException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor.
	 * @param message
	 */
	public CijenaJePreniskaException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * @param cause
	 */
	public CijenaJePreniskaException(Throwable cause) {
		super(cause);
	}

}
