package hr.java.vjezbe.iznimke;

public class BazaPodatakaException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2257111157118760237L;

	/**
	 * Constructor.
	 */
	public BazaPodatakaException() {
		super("Dogodila se pogreška u radu programa!");
	}

	/**
	 * Constructor.
	 * @param message
	 * @param cause
	 */
	public BazaPodatakaException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor.
	 * @param message
	 */
	public BazaPodatakaException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * @param cause
	 */
	public BazaPodatakaException(Throwable cause) {
		super(cause);
	}
}
