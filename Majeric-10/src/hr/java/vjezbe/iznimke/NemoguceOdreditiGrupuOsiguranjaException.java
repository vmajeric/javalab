package hr.java.vjezbe.iznimke;
/**
 * Thrown if insurance group cannot be determined.
 * @author Vjekoslav Majerić
 *
 */
public class NemoguceOdreditiGrupuOsiguranjaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2291535264076831445L;

	/**
	 * Constructor.
	 */
	public NemoguceOdreditiGrupuOsiguranjaException() {
		super("Dogodila se pogreška u radu programa!");
	}

	/**
	 * Constructor.
	 * @param message
	 * @param cause
	 */
	public NemoguceOdreditiGrupuOsiguranjaException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor.
	 * @param message
	 */
	public NemoguceOdreditiGrupuOsiguranjaException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * @param cause
	 */
	public NemoguceOdreditiGrupuOsiguranjaException(Throwable cause) {
		super(cause);
	}

}
